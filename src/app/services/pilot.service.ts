import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { pilot } from '../models/pilot';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class PilotService {

    constructor(private httpClient: HttpClient) { }
  
    addPilot(pilot: pilot): Observable<any> {
      return this.httpClient.post<pilot>('/api/musics', pilot);
    }
  
    getPilot(id: number): Observable<pilot> {
      return this.httpClient.get<pilot>(`/api/pilots/${id}`);
    }
  
    getAllPilots(): Observable<pilot[]> {
      return this.httpClient.get<pilot[]>(`/api/pilots`);
    }
  
    deletePilot(id): Observable<any> {
      return this.httpClient.delete(`/api/pilots/${id}`);
    }
}
