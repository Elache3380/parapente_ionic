import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParagliderPage } from './paraglider.page';

describe('ParagliderPage', () => {
  let component: ParagliderPage;
  let fixture: ComponentFixture<ParagliderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagliderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParagliderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
