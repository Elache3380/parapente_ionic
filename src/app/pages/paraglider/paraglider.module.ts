import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParagliderPageRoutingModule } from './paraglider-routing.module';

import { ParagliderPage } from './paraglider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParagliderPageRoutingModule
  ],
  declarations: [ParagliderPage]
})
export class ParagliderPageModule {}
