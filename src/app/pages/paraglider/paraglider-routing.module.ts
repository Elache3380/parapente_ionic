import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParagliderPage } from './paraglider.page';

const routes: Routes = [
  {
    path: '',
    component: ParagliderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParagliderPageRoutingModule {}
