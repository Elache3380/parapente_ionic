import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilotPage } from './pilot.page';

describe('PilotPage', () => {
  let component: PilotPage;
  let fixture: ComponentFixture<PilotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
