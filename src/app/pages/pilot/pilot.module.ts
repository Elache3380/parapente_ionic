import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotPageRoutingModule } from './pilot-routing.module';

import { PilotPage } from './pilot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotPageRoutingModule
  ],
  declarations: [PilotPage]
})
export class PilotPageModule {}
