import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PilotPage } from './pilot.page';

const routes: Routes = [
  {
    path: '',
    component: PilotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PilotPageRoutingModule {}
