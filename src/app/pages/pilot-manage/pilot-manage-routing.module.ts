import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PilotManagePage } from './pilot-manage.page';

const routes: Routes = [
  {
    path: '',
    component: PilotManagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PilotManagePageRoutingModule {}
