import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilotManagePage } from './pilot-manage.page';

describe('PilotManagePage', () => {
  let component: PilotManagePage;
  let fixture: ComponentFixture<PilotManagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotManagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilotManagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
