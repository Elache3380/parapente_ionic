import { Component, OnInit } from '@angular/core';
import { PilotService } from '../../services/pilot.service';
import { pilot } from 'src/app/models/pilot';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-pilot-manage',
  templateUrl: './pilot-manage.page.html',
  styleUrls: ['./pilot-manage.page.scss'],
})
export class PilotManagePage implements OnInit{
  constructor(private pilotService: PilotService, private fBuilder: FormBuilder) { }

  form: FormGroup;
  pilots: pilot[] = [];

  ngOnInit() {
    this.form = this.fBuilder.group({
      name: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required])
    });
  }

  onSubmitAction() {
    console.log(this.form.value);
    if (this.form.valid) {
      this.pilotService.addPilot(this.form.value).subscribe(data => console.log(data));
    }
  }

  ionViewDidEnter() {
    this.pilotService.getAllPilots().subscribe(data => this.pilots = data);
  }

  get rows(): Array<pilot[]> {
    const aRows = [];

    const nbRows = (this.pilots.length % 4);

    for (let i = 0; i < nbRows; i++) {
      const row = [];
      for (let j = 0; j < 4; j++) {
        if (this.pilots[i + j]) {
          row.push(this.pilots[i + j]);
        }
      }
      aRows.push(row);
    }
    return aRows;
  }
}
