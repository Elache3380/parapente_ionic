import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotManagePageRoutingModule } from './pilot-manage-routing.module';

import { PilotManagePage } from './pilot-manage.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    PilotManagePageRoutingModule
  ],
  declarations: [PilotManagePage]
})
export class PilotManagePageModule {}
