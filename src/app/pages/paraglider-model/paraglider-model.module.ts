import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParagliderModelPageRoutingModule } from './paraglider-model-routing.module';

import { ParagliderModelPage } from './paraglider-model.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParagliderModelPageRoutingModule
  ],
  declarations: [ParagliderModelPage]
})
export class ParagliderModelPageModule {}
