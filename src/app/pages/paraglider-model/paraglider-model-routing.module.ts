import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParagliderModelPage } from './paraglider-model.page';

const routes: Routes = [
  {
    path: '',
    component: ParagliderModelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParagliderModelPageRoutingModule {}
