import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParagliderModelPage } from './paraglider-model.page';

describe('ParagliderModelPage', () => {
  let component: ParagliderModelPage;
  let fixture: ComponentFixture<ParagliderModelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagliderModelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParagliderModelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
