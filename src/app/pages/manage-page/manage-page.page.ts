import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-manage-page',
  templateUrl: './manage-page.page.html',
  styleUrls: ['./manage-page.page.scss'],
})
export class ManagePagePage{

  constructor(public navCtrl: NavController) { }

  
  managePilot() {
    this.navCtrl.navigateRoot("pilot-manage");
  }

}
