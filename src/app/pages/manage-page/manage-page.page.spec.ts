import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManagePagePage } from './manage-page.page';

describe('ManagePagePage', () => {
  let component: ManagePagePage;
  let fixture: ComponentFixture<ManagePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManagePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
