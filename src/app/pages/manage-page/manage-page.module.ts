import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagePagePageRoutingModule } from './manage-page-routing.module';

import { ManagePagePage } from './manage-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagePagePageRoutingModule
  ],
  declarations: [ManagePagePage]
})
export class ManagePagePageModule {}
