import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagePagePage } from './manage-page.page';

const routes: Routes = [
  {
    path: '',
    component: ManagePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagePagePageRoutingModule {}
