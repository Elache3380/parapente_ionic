import { paragliderType } from './paragliderType';

export interface paraglider{

paragliderTypeID: paragliderType["typeId"];
serialNumber: number;
firstFlight: Date;
lastUsed: Date;
}