import { pilot } from './pilot';
import { landingSite } from './landingSite';
import { takeoffSite } from './takeoffSite';


export interface stage{

id: number;
startDate: Date;
endDate: Date;
pilotID: pilot["id"];
landingSite: landingSite["name"];
takeoffSite: takeoffSite["name"]

}