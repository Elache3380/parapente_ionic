import { pilotLevel } from './pilotLevel';

export interface takeoffSite {

    name: string;
    levelId: pilotLevel["id"];
    orientationToLand: string;
}