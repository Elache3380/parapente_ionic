import { pilotLevel } from './pilotLevel';
export interface landingSite{

name: string;
levelId: pilotLevel["id"];
orientationToLand: string;

}