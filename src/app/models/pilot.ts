import { clubRole } from './clubRole';
import { pilotLevel } from './pilotLevel';

export interface pilot {

id: number;
name: string;
firstName: string;
birthDate: Date;
weight: number;
hasPaid: boolean;
firstInscription: Date;
lastSubscription: Date;
clubRole: clubRole["title"];
level: pilotLevel["title"];
}