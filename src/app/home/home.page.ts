import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FlightsPage } from '../pages/flights/flights.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public navCtrl: NavController) {}

  goToFlights() {
    this.navCtrl.navigateRoot("flights");
  }

  goToPilots(){
    this.navCtrl.navigateRoot("pilot");
  }

  goToParagliders(){
    this.navCtrl.navigateRoot("paraglider");
  }

  goToManage(){
    this.navCtrl.navigateRoot("manage-page");
  }
}
