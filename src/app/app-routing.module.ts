import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'manage-page',
    loadChildren: () => import('./pages/manage-page/manage-page.module').then( m => m.ManagePagePageModule)
  },
  {
    path: 'pilot',
    loadChildren: () => import('./pages/pilot/pilot.module').then( m => m.PilotPageModule)
  },
  {
    path: 'paraglider',
    loadChildren: () => import('./pages/paraglider/paraglider.module').then( m => m.ParagliderPageModule)
  },
  {
    path: 'paraglider-model',
    loadChildren: () => import('./pages/paraglider-model/paraglider-model.module').then( m => m.ParagliderModelPageModule)
  },
  {
    path: 'site',
    loadChildren: () => import('./pages/site/site.module').then( m => m.SitePageModule)
  },
  {
    path: 'flights',
    loadChildren: () => import('./pages/flights/flights.module').then( m => m.FlightsPageModule)
  },
  {
    path: 'pilot-manage',
    loadChildren: () => import('./pages/pilot-manage/pilot-manage.module').then( m => m.PilotManagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
